﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Auto_Organizer
{
    class FileIdentifier
    {
        private List<String> extensions;
        private List<String> source;
        private List<String> destination;
        private List<String> exportedText;
        private String FINAL_LIST;
        private String newLine;
        private int count;

        public FileIdentifier(List<String> s, List<String> d, List<String> ext)
        {
            extensions = ext;
            source = s;
            destination = d;
            FINAL_LIST = "";
            newLine = Environment.NewLine;
            count = 0;
            exportedText = new List<String>();
            getFinalList();
        }

        public void ExportToText()
        {
            // Set a variable to the Documents path.
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "WriteLines.txt")))
            {
                for (int i = 0; i < exportedText.Count; i++)
                {
                    outputFile.WriteLine(exportedText[i]);
                }
            }

        }

        public String getFinalList()
        {
            FINAL_LIST += "Source :" + newLine;

            for (int i = 0; i < source.Count; i++)
            {
                String a = source[i];
                FINAL_LIST += "> " + a + " " + newLine;
                exportedText.Add("S:" + source[i]);
                count++;
            }

            FINAL_LIST += "Destination :" + newLine;


            for (int i = 0; i < destination.Count; i++)
            {
                String a = destination[i];
                FINAL_LIST += "> " + a + " " + newLine;
                exportedText.Add("D:" + destination[i]);

                count++;
            }

            FINAL_LIST += "Extensions :" + newLine;


            for (int i = 0; i < extensions.Count; i++)
            {
                String a = extensions[i];
                FINAL_LIST += "> " + a + " " + newLine;
                exportedText.Add("E:" + extensions[i]);
                count++;
            }

            count += 3;

            return FINAL_LIST;
        }

        public String getFinal()
        {
            return FINAL_LIST;
        }
        public int getFinalListCount()
        {
            return count;
        }

    }
}
