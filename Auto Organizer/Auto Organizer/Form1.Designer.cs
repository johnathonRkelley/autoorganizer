﻿namespace Auto_Organizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtExtensions = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.butDel = new System.Windows.Forms.Button();
            this.btnLocation = new System.Windows.Forms.Button();
            this.btnStartOrganizing = new System.Windows.Forms.Button();
            this.lstFinal = new System.Windows.Forms.ListBox();
            this.lstFolders = new System.Windows.Forms.ListBox();
            this.lstExtensions = new System.Windows.Forms.ListBox();
            this.btnFolderAdd = new System.Windows.Forms.Button();
            this.btnFolderDelete = new System.Windows.Forms.Button();
            this.btnAddFinal = new System.Windows.Forms.Button();
            this.btnEditFinal = new System.Windows.Forms.Button();
            this.btnDeleteFinal = new System.Windows.Forms.Button();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblCompletion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folders to Check";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 181);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(243, 41);
            this.label2.TabIndex = 2;
            this.label2.Text = "File Extensions";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtExtensions
            // 
            this.txtExtensions.Location = new System.Drawing.Point(22, 278);
            this.txtExtensions.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtExtensions.MaxLength = 5;
            this.txtExtensions.Name = "txtExtensions";
            this.txtExtensions.Size = new System.Drawing.Size(84, 20);
            this.txtExtensions.TabIndex = 4;
            this.txtExtensions.Text = "txtExtensions";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(148, 278);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(56, 19);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // butDel
            // 
            this.butDel.Location = new System.Drawing.Point(208, 278);
            this.butDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(56, 19);
            this.butDel.TabIndex = 6;
            this.butDel.Text = "btnDel";
            this.butDel.UseVisualStyleBackColor = true;
            this.butDel.Click += new System.EventHandler(this.butDel_Click);
            // 
            // btnLocation
            // 
            this.btnLocation.Location = new System.Drawing.Point(22, 320);
            this.btnLocation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLocation.Name = "btnLocation";
            this.btnLocation.Size = new System.Drawing.Size(130, 19);
            this.btnLocation.TabIndex = 7;
            this.btnLocation.Text = "btnLocation";
            this.btnLocation.UseVisualStyleBackColor = true;
            this.btnLocation.Click += new System.EventHandler(this.btnLocation_Click);
            // 
            // btnStartOrganizing
            // 
            this.btnStartOrganizing.Location = new System.Drawing.Point(22, 388);
            this.btnStartOrganizing.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStartOrganizing.Name = "btnStartOrganizing";
            this.btnStartOrganizing.Size = new System.Drawing.Size(232, 37);
            this.btnStartOrganizing.TabIndex = 8;
            this.btnStartOrganizing.Text = "btnStartOrganizing";
            this.btnStartOrganizing.UseVisualStyleBackColor = true;
            this.btnStartOrganizing.Click += new System.EventHandler(this.btnStartOrganizing_Click);
            // 
            // lstFinal
            // 
            this.lstFinal.FormattingEnabled = true;
            this.lstFinal.Location = new System.Drawing.Point(301, 10);
            this.lstFinal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstFinal.Name = "lstFinal";
            this.lstFinal.Size = new System.Drawing.Size(244, 329);
            this.lstFinal.TabIndex = 9;
            this.lstFinal.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstFinal_DrawItem);
            this.lstFinal.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.lstFinal_MeasureItem);
            // 
            // lstFolders
            // 
            this.lstFolders.FormattingEnabled = true;
            this.lstFolders.Location = new System.Drawing.Point(22, 53);
            this.lstFolders.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstFolders.Name = "lstFolders";
            this.lstFolders.Size = new System.Drawing.Size(244, 95);
            this.lstFolders.TabIndex = 10;
            // 
            // lstExtensions
            // 
            this.lstExtensions.FormattingEnabled = true;
            this.lstExtensions.Location = new System.Drawing.Point(22, 231);
            this.lstExtensions.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstExtensions.Name = "lstExtensions";
            this.lstExtensions.Size = new System.Drawing.Size(244, 43);
            this.lstExtensions.TabIndex = 11;
            // 
            // btnFolderAdd
            // 
            this.btnFolderAdd.Location = new System.Drawing.Point(22, 152);
            this.btnFolderAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFolderAdd.Name = "btnFolderAdd";
            this.btnFolderAdd.Size = new System.Drawing.Size(83, 19);
            this.btnFolderAdd.TabIndex = 12;
            this.btnFolderAdd.Text = "btnFolderAdd";
            this.btnFolderAdd.UseVisualStyleBackColor = true;
            this.btnFolderAdd.Click += new System.EventHandler(this.btnFolderAdd_Click);
            // 
            // btnFolderDelete
            // 
            this.btnFolderDelete.Location = new System.Drawing.Point(173, 152);
            this.btnFolderDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFolderDelete.Name = "btnFolderDelete";
            this.btnFolderDelete.Size = new System.Drawing.Size(92, 19);
            this.btnFolderDelete.TabIndex = 13;
            this.btnFolderDelete.Text = "btnFolderDelete";
            this.btnFolderDelete.UseVisualStyleBackColor = true;
            this.btnFolderDelete.Click += new System.EventHandler(this.btnFolderDelete_Click);
            // 
            // btnAddFinal
            // 
            this.btnAddFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFinal.Location = new System.Drawing.Point(301, 344);
            this.btnAddFinal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddFinal.Name = "btnAddFinal";
            this.btnAddFinal.Size = new System.Drawing.Size(23, 19);
            this.btnAddFinal.TabIndex = 14;
            this.btnAddFinal.Text = "+";
            this.btnAddFinal.UseVisualStyleBackColor = true;
            this.btnAddFinal.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnEditFinal
            // 
            this.btnEditFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditFinal.Location = new System.Drawing.Point(356, 344);
            this.btnEditFinal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEditFinal.Name = "btnEditFinal";
            this.btnEditFinal.Size = new System.Drawing.Size(23, 19);
            this.btnEditFinal.TabIndex = 15;
            this.btnEditFinal.Text = "<";
            this.btnEditFinal.UseVisualStyleBackColor = true;
            // 
            // btnDeleteFinal
            // 
            this.btnDeleteFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteFinal.Location = new System.Drawing.Point(328, 344);
            this.btnDeleteFinal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDeleteFinal.Name = "btnDeleteFinal";
            this.btnDeleteFinal.Size = new System.Drawing.Size(23, 19);
            this.btnDeleteFinal.TabIndex = 16;
            this.btnDeleteFinal.Text = "-";
            this.btnDeleteFinal.UseVisualStyleBackColor = true;
            this.btnDeleteFinal.Click += new System.EventHandler(this.btnDeleteFinal_Click);
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(19, 347);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(0, 13);
            this.lblLocation.TabIndex = 17;
            // 
            // lblCompletion
            // 
            this.lblCompletion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompletion.Location = new System.Drawing.Point(298, 388);
            this.lblCompletion.Name = "lblCompletion";
            this.lblCompletion.Size = new System.Drawing.Size(247, 35);
            this.lblCompletion.TabIndex = 18;
            this.lblCompletion.Text = "label3";
            this.lblCompletion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 441);
            this.Controls.Add(this.lblCompletion);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.btnDeleteFinal);
            this.Controls.Add(this.btnEditFinal);
            this.Controls.Add(this.btnAddFinal);
            this.Controls.Add(this.btnFolderDelete);
            this.Controls.Add(this.btnFolderAdd);
            this.Controls.Add(this.lstExtensions);
            this.Controls.Add(this.lstFolders);
            this.Controls.Add(this.lstFinal);
            this.Controls.Add(this.btnStartOrganizing);
            this.Controls.Add(this.btnLocation);
            this.Controls.Add(this.butDel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtExtensions);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Auto Organizer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtExtensions;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.Button btnLocation;
        private System.Windows.Forms.Button btnStartOrganizing;
        private System.Windows.Forms.ListBox lstFinal;
        private System.Windows.Forms.ListBox lstFolders;
        private System.Windows.Forms.ListBox lstExtensions;
        private System.Windows.Forms.Button btnFolderAdd;
        private System.Windows.Forms.Button btnFolderDelete;
        private System.Windows.Forms.Button btnAddFinal;
        private System.Windows.Forms.Button btnEditFinal;
        private System.Windows.Forms.Button btnDeleteFinal;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblCompletion;
    }
}

