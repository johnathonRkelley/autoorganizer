﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Auto_Organizer
{
    public partial class Form1 : Form
    {
        private string FINAL_LIST;
        private int FINAL_LIST_COUNT;
        private List<FileIdentifier> files;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lstFinal.DrawMode = DrawMode.OwnerDrawVariable;
            FINAL_LIST = "";
            FINAL_LIST_COUNT = 0;
            txtExtensions.Text = "";
            files = new List<FileIdentifier>();

            //Changes Design Variables From Form
            btnFolderAdd.Text = "Add";
            btnFolderDelete.Text = "Delete";
            btnAdd.Text = "Add";
            butDel.Text = "Delete";
            btnLocation.Text = "Final Location";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtExtensions.Text != "")
            {
                lstExtensions.Items.Add(txtExtensions.Text);
                txtExtensions.Text = "";
            }
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            int index = lstExtensions.SelectedIndex;

            lstExtensions.Items.RemoveAt(index);
        }

        private void btnFolderAdd_Click(object sender, EventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            dialog.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog(); 
            if (result == DialogResult.OK)
            {
                String path = dialog.SelectedPath;
                lstFolders.Items.Add(path);
                Console.WriteLine(path);
            }
        }

        private void btnFolderDelete_Click(object sender, EventArgs e)
        {
            int index = lstExtensions.SelectedIndex;

            lstExtensions.Items.RemoveAt(index);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //string newLine = Environment.NewLine;
            //lstFinal.Items.Add("Source : " + newLine + "Destination : " + newLine + "Extensions : ");
            getFinalListInformation();
        }

        private void lstFinal_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            //e.ItemHeight = 39;
            e.ItemHeight = 13 * FINAL_LIST_COUNT;
        }

        private void lstFinal_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.Graphics.DrawString(lstFinal.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds);
        }

        public void getFinalListInformation()
        {
            if (lstFolders.Items.Count > 0 && lstExtensions.Items.Count > 0 && lblLocation.Text != "")
            {
                string newLine = Environment.NewLine;

                List<String> source = new List<String>();
                List<String> destination = new List<String>();
                List<String> extensions = new List<String>();

                for (int i = 0; i < lstFolders.Items.Count; i++)
                {
                    source.Add(lstFolders.Items[i].ToString());
                    Console.WriteLine(lstFolders.Items[i]);
                }

                for (int i = 0; i < lstExtensions.Items.Count; i++)
                {
                    extensions.Add(lstExtensions.Items[i].ToString());

                    Console.WriteLine(lstExtensions.Items[i]);
                }

                destination.Add(lblLocation.Text);

                //Clears all data
                lstFolders.Items.Clear();
                lstExtensions.Items.Clear();
                lblLocation.Text = "";

                FileIdentifier item = new FileIdentifier(source, destination, extensions);
                Console.WriteLine("Line count : " + item.getFinalListCount());
                Console.WriteLine(item.getFinal());

                files.Add(item);

                FINAL_LIST = item.getFinal();
                FINAL_LIST_COUNT = item.getFinalListCount();
                lstFinal.Items.Add(FINAL_LIST);
            }
            else
            {
                //Do Nothing
            }
            
        }

        private void btnDeleteFinal_Click(object sender, EventArgs e)
        {
            if (lstFinal.Items.Count > 0)
            {
                int index = lstFinal.SelectedIndex;
                lstFinal.Items.RemoveAt(index);
                files.RemoveAt(index);
            }

        }

        private void btnLocation_Click(object sender, EventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            dialog.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                String path = dialog.SelectedPath;
                Console.WriteLine(path);
                lblLocation.Text = path;
            }
        }

        private void btnStartOrganizing_Click(object sender, EventArgs e)
        {
            int count = files.Count;

            for (int i = 0; i < files.Count; i++)
            {
                files[i].ExportToText();
            }
            files.Clear();

            lstFinal.Items.Clear();

            lblCompletion.Text = "Completed - " + count + " was sent to the text file WriteLines.txt in My Documents.";
        }
    }
}
